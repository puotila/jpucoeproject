# Generating point values in the range 0.0 - 100.0 and
# write them to the file "data.txt" one value per line.

from random import random

f = open('data.txt', 'w')

for i in range(100):
        value = "%6.2f\n" % (100.0 * random())
        f.write(value)

f.close()
